from django.urls import path
from hello_world import views

urlpatterns = [
    path('hello/', views.hello_world, name='hello_world'),
    path('4ever/', views.ls, name='ls'),
]
