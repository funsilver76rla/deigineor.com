from django.shortcuts import render

def hello_world(requests):
    return render(requests, 'hello_world.html', {})

def ls(requests):

    output = ['я','лублю','Катю']
    context = {
        "output": output
    }
    return render(requests, 'ls.html', context)

