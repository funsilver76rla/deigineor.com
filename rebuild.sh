#!/bin/bash
echo "[--] DEIGINEOR SITE CI"
echo "[..] runnign linter for content.yml"

yamllint -c .yamllint content.yml

if [ "$?" != 0 ]; then
	echo "[!!] Got an error in checking content.yml"
	echo "[  ] Please check any errors above and fix them"
	echo ""
	echo "[!!] Site will not be redeployed!" 
	exit 1
else 
	echo "[ok] content.yml seems to be ok..."
	echo "[..] redeploying..."
	# docker stop deigineor.com || echo "[??] deigineor.com was not running"
        # docker-compose -f docker-compose.deigineor.yml stop
	echo "[..] build container..."
	# docker build . -t deigineor.com:latest
        docker-compose -f docker-compose.deigineor.yml build
	echo "[ok] builded!"
	echo ""
	echo "[..] run container..."
	# docker run --name deigineor.com --rm --network docker_default -d deigineor.com
        docker-compose -f docker-compose.deigineor.yml up -d --force-recreate
	echo "[ok] runned!"
	# docker logs deigineor.com
        docker-compose -f docker-compose.deigineor.yml logs 
	echo ""
fi
echo "[OK] DEIGINEOR Site repeloyed!"
