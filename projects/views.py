from django.shortcuts import render
from django.conf import settings

from projects.models import Project
import yaml

def project_index(request):
    projects = Project.objects.all()
    context = {
        'projects': projects
    }
    return render(request, 'project_index.html', context)

def project_detail(request, pk):
    project = Project.objects.get(pk=pk)
    context = {
        'project': project
    }
    return render(request, 'project_detail.html', context)

def read_config(config_file):
    with open(config_file, 'r') as file_descriptor:
        config = yaml.safe_load(file_descriptor)
    file_descriptor.close()
    return config

def chunker_list(l, n):
    # https://www.geeksforgeeks.org/break-list-chunks-size-n-python/
    return [l[i:i + n] for i in range(0, len(l), n)]

def tracks(request, genre="All", page=1):
    # config = read_config('/home/shellshock/dNull/www/deigineor.com/content.yml')
    per_page = 3
    config = read_config(settings.BASE_DIR + '/content.yml')
    genre_track_count = 0

    genre_tracks = []
    # Get genre track count
    for track in config["tracks"]:
        if genre == "All" or track["genre"] == genre:
            genre_track_count += 1
            genre_tracks.append(track)

    genre_tracks_chunks = chunker_list(genre_tracks, per_page)
    # from pprint import pprint
    # pprint(genre_tracks_per_page[0])
    try:
        tracks_on_current_page = genre_tracks_chunks[page - 1]
    except IndexError:
        tracks_on_current_page = []
        page = 0
    page_count = len(genre_tracks_chunks)
    print(page_count)

    if page == 1:
        prev_page = 0
    else:
        prev_page = page - 1
    if page >= page_count:
        next_page = 0
    else:
        next_page = page + 1

    context = {
        'c': config,
        'genre': genre,
        'track_count': genre_track_count,
        'tracks': tracks_on_current_page,
        'page_count': range(1, page_count + 1),
        'page': page,
        'prev_page': prev_page,
        'next_page': next_page
    }
    return render(request, 'tracks.html', context)



def landing(request):
    config = read_config(settings.BASE_DIR + '/content.yml')
    context = {
        'c': config
    }
    return render(request, 'landing.html', context)

def bio(request):
    config = read_config(settings.BASE_DIR + '/content.yml')
    context = {
        'c': config
    }
    return render(request, 'bio.html', context)

def gallery(request):
    config = read_config(settings.BASE_DIR + '/content.yml')
    context = {
        'c': config
    }
    return render(request, 'gallery.html', context)

def contacts(request):
    config = read_config(settings.BASE_DIR + '/content.yml')
    context = {
        'c': config
    }
    return render(request, 'contacts.html', context)

