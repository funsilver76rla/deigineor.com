from django.urls import path
from . import views

urlpatterns = [
    path("", views.landing, name="landing"),
    path("tracks/", views.tracks, name="tracks"),
    path("tracks/<str:genre>/<int:page>", views.tracks, name="genre"),
    path("bio/", views.bio, name="bio"),
    path("gallery/", views.gallery, name="gallery"),
    path("contacts/", views.contacts, name="contacts"),
]
