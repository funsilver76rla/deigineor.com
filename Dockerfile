FROM django:onbuild

COPY . /usr/src/app
RUN pip install

EXPOSE 8000
